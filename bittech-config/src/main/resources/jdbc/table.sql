create table if not exists `t_config`(
	id int(8) primary key auto_increment comment '环境id',
	subarea_name varchar(32) NOT NULL default '',
	profile_name varchar(32) NOT NULL default '',
	label varchar(64) NOT NULL comment '等价于key',
	value varchar(128) NOT NULL default "" comment 'label对应的value值'
)engine innodb;