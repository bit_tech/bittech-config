package com.bittech.config;

import java.io.InputStream;

public abstract class AbstractFileResources<T> implements Resources {

	protected T t;

	/**
	 * 
	 * @param subareaName
	 * @param profileName
	 * @param basePath
	 * @param fileName
	 * @return
	 */
	protected String compoundPath(String subareaName, String profileName, String basePath, String fileName,
			String suffix) {
		StringBuffer sb = new StringBuffer();
		if (StringUtils.hasLength(basePath)) {
			sb.append(basePath).append("/");
		}
		if (StringUtils.hasLength(subareaName)) {
			sb.append(subareaName).append("/");
		}
		if (StringUtils.hasLength(fileName)) {
			sb.append(fileName);
			if (!fileName.contains(suffix) && StringUtils.hasLength(suffix)) {
				sb.append(suffix);
			}
		}
		if (StringUtils.hasLength(profileName)) {
			int index = sb.lastIndexOf(".");
			sb.insert(index, new StringBuffer("_").append(profileName));
		}
		return sb.toString();
	}

	public abstract String getSuffix();

	/**
	 * 加载文件
	 * 
	 * @param subareaName 分区名
	 * @param profileName 环境名
	 * @param basePath    公共路径
	 * @param fileName    文件名
	 * @return
	 */
	public abstract T loadFile(String subareaName, String profileName, String basePath, String fileName);

	/**
	 * 加载本地文件
	 * 
	 * @param path
	 * @return
	 */
	public abstract T loadNativeFile(String path);

	protected abstract T readFile(InputStream is);

	public abstract Object get(String key);

}
