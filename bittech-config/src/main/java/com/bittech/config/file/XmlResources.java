package com.bittech.config.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import com.bittech.config.AbstractFileResources;

public class XmlResources extends AbstractFileResources<Properties> {

	private static final String SUFFIX = ".xml";

	@Override
	public Properties loadFile(String subareaName, String profileName, String basePath, String fileName) {
		InputStream is = null;
		try {
			String path = compoundPath(subareaName, profileName, basePath, fileName, SUFFIX);
			is = ClassLoader.getSystemResourceAsStream(path);
			return (t = this.readFile(is));
		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Properties loadNativeFile(String path) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(path);
			return (t = this.readFile(fis));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null) {
					fis.close();
					fis = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected Properties readFile(InputStream inputStream) {
		try {
			Properties properties = new Properties();
			properties.loadFromXML(inputStream);
			return properties;
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object get(String key) {
		return t.get(key);
	}

	@Override
	public String getSuffix() {
		return SUFFIX;
	}

}
