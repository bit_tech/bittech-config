package com.bittech.config.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.bittech.config.AbstractFileResources;

public class PropertiesResources extends AbstractFileResources<Properties> {

	private static final String SUFFIX = ".properties";

	@Override
	public Properties loadFile(String subareaName, String profileName, String basePath, String fileName) {
		InputStream is = null;
		try {
			String path = compoundPath(subareaName, profileName, basePath, fileName, SUFFIX);
			is = ClassLoader.getSystemResourceAsStream(path);
			return this.readFile(is);
		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Properties loadNativeFile(String subareaName, String profileName, String basePath, String fileName) {
		String path = compoundPath(subareaName, profileName, basePath, fileName, SUFFIX);
		return loadNativeFile(path);
	}

	@Override
	public Properties loadNativeFile(String path) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(path);
			return this.readFile(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null) {
					fis.close();
					fis = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected Properties readFile(InputStream is) {
		try {
			Properties properties = new Properties();
			properties.load(is);
			return (t = properties);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object get(String key) {
		return t.get(key);
	}

	@Override
	public String getSuffix() {
		return SUFFIX;
	}

}
