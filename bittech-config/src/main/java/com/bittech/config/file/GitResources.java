package com.bittech.config.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;

import com.bittech.config.AbstractFileResources;

public class GitResources<T> extends AbstractFileResources<T> {

	private String repositoryUrl;
	private AbstractFileResources<T> abstractFileResources;

	public GitResources(String repositoryUrl, AbstractFileResources<T> abstractFileResources) {
		this.repositoryUrl = repositoryUrl;
		this.abstractFileResources = abstractFileResources;
		this.getWorkSpace(repositoryUrl);
	}

	@Override
	public T loadFile(String subareaName, String profileName, String basePath, String fileName) {
		try {
			this.updateConfig(basePath);
			String path = compoundPath(subareaName, profileName, basePath, fileName, abstractFileResources.getSuffix());
			return abstractFileResources.loadNativeFile(path);
		} catch (InvalidRemoteException e) {
			e.printStackTrace();
		} catch (TransportException e) {
			e.printStackTrace();
		} catch (GitAPIException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean updateConfig(String basePath)
			throws InvalidRemoteException, TransportException, GitAPIException, IOException {
		if (repositoryBare(basePath)) {
			// 空
			CloneCommand cloneCommand = Git.cloneRepository();
			cloneCommand.setURI(repositoryUrl);
			cloneCommand.setDirectory(new File(basePath));
			cloneCommand.call();
		} else {
			// 非空
			PullCommand pullCommand = git(basePath).pull();
			pullCommand.call();
		}
		return true;
	}

	/**
	 * 判断一个目录是不是空的git仓库
	 * 
	 * @param basePath
	 * @return
	 */
	private boolean repositoryBare(String basePath) {
		try {
			// String codePath = codePath(basePath);
			Git git = git(basePath);
			return git.getRepository().isBare();
		} catch (IOException e) {
			return true;
		}
	}

//	private String codePath(String basePath) {
//		return new StringBuffer(basePath).append(projectName).toString();
//	}

	private Git git(String basePath) throws IOException {
		return Git.open(new File(basePath));
	}

	/**
	 * 根据git的url获取工作空间
	 * 
	 * @param url
	 * @return
	 */
	private String getWorkSpace(String url) {
		int beginIndex = url.lastIndexOf("/");
		int endIndex = url.lastIndexOf(".");
		return url.substring(beginIndex, endIndex);
	}

	@Override
	public T loadNativeFile(String path) {
		return null;
	}

	@Override
	protected T readFile(InputStream is) {
		return null;
	}

	@Override
	public Object get(String key) {
		return null;
	}

	@Override
	public String getSuffix() {
		return null;
	}

}
