package com.bittech.config.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import com.bittech.config.AbstractFileResources;

public class YamlResources extends AbstractFileResources<Map<String, Object>> {

	private static final String SUFFIX = ".yaml";

	@Override
	public Map<String, Object> loadFile(String subareaName, String profileName, String basePath, String fileName) {
		String path = compoundPath(subareaName, profileName, basePath, fileName, SUFFIX);
		InputStream is = null;
		try {
			is = ClassLoader.getSystemResourceAsStream(path);
			return this.readFile(is);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Map<String, Object> loadNativeFile(String path) {
		FileInputStream fis = null;
		try {
			File file = new File(path);
			fis = new FileInputStream(file);
			return this.readFile(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null) {
					fis.close();
					fis = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected Map<String, Object> readFile(InputStream is) {
		Yaml yaml = new Yaml(new Constructor(Map.class));
		return (t = yaml.load(is));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object get(String key) {
		String[] keys = key.split(":");
		Map<String, Object> map = t;
		Object value = null;
		for (String k : keys) {
			value = map.get(k);
			if (value instanceof Map) {
				map = (Map<String, Object>) value;
			}
		}
		return value;
	}

	@Override
	public String getSuffix() {
		return SUFFIX;
	}

}
