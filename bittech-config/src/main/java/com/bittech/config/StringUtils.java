package com.bittech.config;

public class StringUtils {

	public static boolean hasLength(String text) {
		return text != null && !"".equals(text);
	}

}
