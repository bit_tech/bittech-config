package com.bittech.config.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;

public class ConfigServer {

	public static void main(String[] args) throws InvalidRemoteException, TransportException, GitAPIException {
		try {
			socket();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void socket() throws UnknownHostException, IOException, InvalidRemoteException, TransportException, GitAPIException {
		// git clone file:///D/project/servlet/bittech-config/.git
		Socket socket = new Socket();
		socket.connect(new InetSocketAddress("localhost", 8080), 1000);

		StringBuffer httpHeader = new StringBuffer();
		httpHeader.append("GET /bittech-hello-web/user?count=5 HTTP/1.1\r\n");
		httpHeader.append("Host: localhost\r\n");
		// httpHeader.append("Content-Type:text/html\r\n");
		httpHeader.append("\r\n");
		OutputStream os = socket.getOutputStream();
		os.write(httpHeader.toString().getBytes());

		InputStream is = socket.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		StringBuffer data = new StringBuffer();
		String temp = null;
		while ((temp = br.readLine()) != null) {
			data.append(temp);
		}
		System.out.println(data.toString());
	}

}
