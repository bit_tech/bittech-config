package com.bittech.config;

public class NoSuchResourceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2443717817804049355L;

	public NoSuchResourceException() {
		super();
	}

	public NoSuchResourceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public NoSuchResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSuchResourceException(String message) {
		super(message);
	}

	public NoSuchResourceException(Throwable cause) {
		super(cause);
	}

}
