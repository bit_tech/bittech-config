package com.bittech.config.remote;

import com.bittech.config.AbstractRemoteResources;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

public class RedisResources extends AbstractRemoteResources {

	private static StatefulRedisConnection<String, String> connect;
	static {
		RedisURI redisURI = RedisURI.create("redis://localhost/");
		redisURI.setDatabase(0);
		redisURI.setPassword("");
		redisURI.setPort(6379);
		RedisClient redisClient = RedisClient.create(redisURI);
		connect = redisClient.connect();
	}

	@Override
	public String get(String subareaName, String profileName, String label) {
		StringBuffer key1 = new StringBuffer(BASE);
		key1.append(":").append(subareaName);
		key1.append(":").append(profileName);

		RedisCommands<String, String> redisCommands = connect.sync();
		return redisCommands.hget(key1.toString(), label);
	}

	@Override
	public int addConfig(String subareaName, String profileName, String label, String value) {
		StringBuffer key = new StringBuffer(BASE);
		key.append(":").append(subareaName);
		key.append(":").append(profileName);

		RedisCommands<String, String> redisCommands = connect.sync();
		redisCommands.hset(key.toString(), label, value);
		return 1;
	}

}
