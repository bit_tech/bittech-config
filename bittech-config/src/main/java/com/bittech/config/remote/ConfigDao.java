package com.bittech.config.remote;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConfigDao {

	public int add(Config config) {
		String sql = "insert into t_config(subarea_name,profile_name,label,value) values (?,?,?,?)";
		try {
			PreparedStatement preparedStatement = DBUtils.getConnotion().prepareStatement(sql);
			preparedStatement.setString(1, config.getSubareaName());
			preparedStatement.setString(2, config.getProfileName());
			preparedStatement.setString(3, config.getLabel());
			preparedStatement.setString(4, config.getValue());
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int delete(String subareaName) {
		return 0;
	}

	public int update() {
		return 0;
	}

	public Config find(Config config) {
		try {
			String sql = "select * from t_config where subarea_name = ? and profile_name = ? and label = ?";
			PreparedStatement preparedStatement = DBUtils.getConnotion().prepareStatement(sql);
			preparedStatement.setString(1, config.getSubareaName());
			preparedStatement.setString(2, config.getProfileName());
			preparedStatement.setString(3, config.getLabel());
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();
			if (resultSet.next()) {
				config.setValue(resultSet.getString("value"));
			}
			return config;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

}
