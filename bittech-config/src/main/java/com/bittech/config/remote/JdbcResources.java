package com.bittech.config.remote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bittech.config.AbstractRemoteResources;
import com.bittech.config.StringUtils;

public class JdbcResources extends AbstractRemoteResources {

	private static ConfigDao configDao = new ConfigDao();

	public JdbcResources() {
		this(null);
	}

	public JdbcResources(String dbName) {
		try {
			this.initDB(dbName);
			this.initTable();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String get(String subareaName, String profileName, String label) {
		Config config = new Config();
		config.setSubareaName(subareaName);
		config.setProfileName(profileName);
		config.setLabel(label);
		config = configDao.find(config);
		return config.getValue();
	}

	/**
	 * 创建一个配置的数据库，如果存在就不再创建
	 *
	 * @param dbName 数据库的名字
	 * @return
	 * @throws InterruptedException
	 */
	private boolean initDB(String dbName) throws InterruptedException {
		Statement statement = null;
		try {
			statement = DBUtils.getConnotion().createStatement();
			List<String> sqls = getClasspathFile("jdbc/db.sql");
			for (String sql : sqls) {
				if (StringUtils.hasLength(dbName)) {
					sql.replace(BASE, dbName);
				}
				statement.execute(sql.toString());
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.close(statement);
		}
		return false;
	}

	/**
	 * 创建配置库的表
	 *
	 * @return
	 * @throws InterruptedException
	 */
	private boolean initTable() throws InterruptedException {
		Statement statement = null;
		try {
			statement = DBUtils.getConnotion().createStatement();
			List<String> sqls = this.getClasspathFile("jdbc/table.sql");
			for (String sql : sqls) {
				statement.execute(sql.toString());
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.close(statement);
		}
		return false;
	}

	private List<String> getClasspathFile(String classpath) {
		List<String> list = new ArrayList<>();
		InputStream is = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader bufferedReader = null;
		try {
			is = ClassLoader.getSystemResourceAsStream(classpath);
			inputStreamReader = new InputStreamReader(is, "utf-8");
			bufferedReader = new BufferedReader(inputStreamReader);
			StringBuffer tableSql = new StringBuffer();
			for (String line; (line = bufferedReader.readLine()) != null;) {
				tableSql.append(line);
				if (StringUtils.hasLength(line) && line.contains(";")) {
					list.add(tableSql.toString());
					tableSql.delete(0, tableSql.length());
				}
			}
			return list;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
					bufferedReader = null;
				}
				if (inputStreamReader != null) {
					inputStreamReader.close();
					inputStreamReader = null;
				}
				if (is != null) {
					is.close();
					is = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public int addConfig(String subareaName, String profileName, String label, String value) {
		Config config = new Config();
		config.setSubareaName(subareaName);
		config.setProfileName(profileName);
		config.setLabel(label);
		config.setValue(value);
		return configDao.add(config);
	}

}
