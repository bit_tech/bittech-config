package com.bittech.config.remote;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBUtils {

    private static final String URL = "jdbc:mysql://localhost:3306/config";
    //private static final LinkedBlockingQueue<Connection> CONNECTIONS = new LinkedBlockingQueue<>();
    private static Connection CONNECTION = null;

    static {
        Properties properties = new Properties();
        properties.put("user", "root");
        properties.put("password", "");
        properties.put("useUnicode", "true");
        properties.put("useSSL", "false");
        properties.put("characterEncoding", "UTF-8");

        try {
            CONNECTION = DriverManager.getConnection(URL, properties);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnotion() throws InterruptedException {
        return CONNECTION;
    }

    public static void close(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
                statement = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
