package com.bittech.config.remote;

public class Config {
	/**
	 * 主键
	 */
	private int id;
	/**
	 * 分区名
	 */
	private String subareaName;
	/**
	 * 环境名
	 */
	private String profileName;
	/**
	 * 等价于key
	 */
	private String label;
	/**
	 * key对应的value
	 */
	private String value;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getSubareaName() {
		return subareaName;
	}

	public void setSubareaName(String subareaName) {
		this.subareaName = subareaName;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
