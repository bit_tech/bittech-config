package com.bittech.config;

public abstract class AbstractRemoteResources implements Resources {

	/**
	 * 获取指定key值
	 * 
	 * @param key
	 * @param subareaName 分区名
	 * @param profileName 环境名
	 * @return
	 */
	public abstract String get(String subareaName, String profileName, String label);

	/**
	 * 添加配置
	 * 
	 * @param subareaName 分区名
	 * @param profileName 环境名
	 * @param label       等价于key
	 * @param value       key对应的value
	 * @return
	 */
	public abstract int addConfig(String subareaName, String profileName, String label, String value);

}
