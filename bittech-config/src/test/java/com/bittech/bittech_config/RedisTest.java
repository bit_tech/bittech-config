package com.bittech.bittech_config;

import org.junit.Test;

import com.bittech.config.remote.RedisResources;

public class RedisTest {

	private static RedisResources redisResources = new RedisResources();

	@Test
	public void testAdd() {
		redisResources.addConfig("b_project", "dev", "msyql", "我是muysql");
	}

	@Test
	public void testFind() {
		String value = redisResources.get("b_project", "dev", "msyql");
		System.out.println(value);
	}

}
