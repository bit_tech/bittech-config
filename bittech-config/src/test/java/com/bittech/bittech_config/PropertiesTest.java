package com.bittech.bittech_config;

import org.junit.Test;

import com.bittech.config.file.PropertiesResources;

public class PropertiesTest {

	private static PropertiesResources propertiesResources = new PropertiesResources();

	@Test
	public void test01() {
		propertiesResources.loadFile("test", "dev", null, "test");
		String value = (String) propertiesResources.get("a.b");
		System.out.println(value);
	}

}
