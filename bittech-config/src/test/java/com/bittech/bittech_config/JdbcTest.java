package com.bittech.bittech_config;

import org.junit.Before;
import org.junit.Test;

import com.bittech.config.remote.JdbcResources;

public class JdbcTest {

	private JdbcResources jdbcResources = null;

	@Before
	public void init() {
		jdbcResources = new JdbcResources();
	}

	@Test
	public void addConfig() {
		jdbcResources.addConfig("a_project", "dev", "redis", "我是redis");
	}

	@Test
	public void find() {
		String value = jdbcResources.get("a_project", "dev", "redis");
		System.out.println(value);
	}

}
