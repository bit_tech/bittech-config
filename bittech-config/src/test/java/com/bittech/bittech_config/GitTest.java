package com.bittech.bittech_config;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.junit.Test;

import com.bittech.config.file.GitResources;
import com.bittech.config.file.PropertiesResources;

public class GitTest {

	private static final String url = "https://gitee.com/bit_tech/bittech-config.git";
	private PropertiesResources propertiesResources = new PropertiesResources();

	public void test01() throws InvalidRemoteException, TransportException, GitAPIException {
		// CloneCommand cloneCommand = Git.cloneRepository();
		// cloneCommand.setURI(url);
		// cloneCommand.setDirectory(new File("C:\\Users\\xcc13\\Desktop\\vv\\bb"));
		// cloneCommand.call();
	}

	public void test02() throws IOException {
		String url = "https://gitee.com/bit_tech/bittech-config.git";
		int beginIndex = url.lastIndexOf("/");
		int endIndex = url.lastIndexOf(".");
		String project = url.substring(beginIndex, endIndex);
		System.out.println(project);
		Git git = Git.open(new File("D:\\project\\servlet" + project));// .lsRemoteRepository();
		System.out.println(git.lsRemote().getRepository().isBare());
	}

	@Test
	public void test03() {
		GitResources<Properties> gitResources = new GitResources<>(url, propertiesResources);
		Properties properties = gitResources.loadFile("test", "dev", "D:/config", "test");
		String value = String.valueOf(properties.get("a.b"));
		System.out.println(value);
	}

}
